package pp2.stockeando.notifier;

import Validator.EmailValidator;
import pp2.stockeando.model.UserContact;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class NotifierEmail implements Notifier {

	@Override
	public boolean send(String message, UserContact receiver) {
		try {
			
			// Propiedades de la conexion
			Properties props = new Properties();
			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.starttls.enable", "true");
			props.setProperty("mail.smtp.port", "587");
			props.setProperty("mail.smtp.user", "stockeando.pp2@gmail.com");
			props.setProperty("mail.smtp.auth", "true");

			
			Session session = Session.getDefaultInstance(props);
			MimeMessage message2 = new MimeMessage(session);
			message2.setFrom(new InternetAddress("stockeando.pp2@gmail.com"));
			
			
			message2.addRecipients(Message.RecipientType.BCC,receiver.getContactDetail());
			message2.setSubject("Notificacion de Stock");
			message2.setText(message);
				
			// Lo enviamos
			Transport t = session.getTransport("smtp");
			t.connect("stockeando.pp2@gmail.com", "pp2Stockeando2");
			t.sendMessage(message2, message2.getAllRecipients());

			// Cierre
			t.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String getNameNotifier() {
		return "Email";
	}

	@Override
	public boolean isReceiverInfoValid(UserContact receiver) {
		return EmailValidator.isMailValid(receiver.getContactDetail());
	}

	@Override
	public int compareTo(Notifier o) {
		return this.getNameNotifier().compareTo(o.getNameNotifier());
	}
}
